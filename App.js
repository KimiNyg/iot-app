import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity, Alert, TextInput } from 'react-native';
import Alarm from './components/AlarmComponent'

export default class App extends Component {

  state = { 
    alarm:[],
    hours: '',
    minutes: ''
  }
  
  handleHours = (text) => {
    this.setState({ hours: text })
  }
  
  handleMinutes = (text) => {
    this.setState({ minutes: text })
  }

  componentDidMount(){
    this.getAlarmsFromApi()
  }

  getAlarmsFromApi = () => {
    return fetch('http://192.168.1.108:3000/api/alarmModel/allAlarms')
      .then((response) => response.json())
      .then((json) => {
        this.setState({ alarm: json })
        return json;
      })
      .catch((error) => {
        console.error(error);
      });
  };

  postAlarm = (hours,minutes) => {
    return fetch('http://192.168.1.108:3000/api/alarmModel/newAlarm', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        hours: hours,
        minutes: minutes,
        active:true
      })
    }).then((response) => response.json())
    .then((json) => {
      arr = this.state.alarm
      arr.push(json)
      this.setState({alarm:arr})
      return json;
    })
    .catch((error) => {
      console.error(error);
    });
  };

  renderAlarms() {
    return this.state.alarm.map((item) => {
        return (
          <Alarm
          key = {item._id}
          id = {item._id}
          hour = {item.hours}
          minute = {item.minutes}
          active = {item.active}>
          </Alarm>
        );
    });
}

clickEvent = (hours, minutes) => {
  alert('hours: ' + hours + ' minutes: ' + minutes)
}

render(){
  return (
    <View style={this.styles.container}>
      <View style={this.styles.addAlarm}>
      <TextInput style={this.styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Hours"
                    placeholderTextColor = "#9a73ef"
                    autoCapitalize = "none"
                    onChangeText = {this.handleHours}/>

            <TextInput style={this.styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Minutes"
                    placeholderTextColor = "#9a73ef"
                    autoCapitalize = "none"
                    onChangeText = {this.handleMinutes}/>

            <TouchableOpacity style={this.styles.button}
                    onPress = {
                        () => this.postAlarm(this.state.hours, this.state.minutes)
                    }>
                    <Text style={this.styles.text}> SUBMIT </Text>
                  </TouchableOpacity>
      </View>
      
      {
        this.renderAlarms()
      }

      <TouchableOpacity
        style={this.styles.updateButton}
        onPress = {
          () => this.getAlarmsFromApi()
        }>
        <Text style={this.styles.text}> UPDATE </Text>
      </TouchableOpacity>

    </View>
  );
}
styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: "center",
    backgroundColor: "#1EA896",
    padding: 10,
    margin:10,
    width:150,
    height:50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  updateButton: {
    position:"absolute",
    bottom: 0,
    alignItems: "center",
    backgroundColor: "#1EA896",
    padding: 10,
    margin:10,
    width:150,
    height:50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
    fontSize:16,
    color:"#4C5454"
  },
  input:{
    fontSize:24,
    margin:10,
    color:"#000"
  },
  addAlarm:{
    position:"absolute",
    top: 0,
    fontSize:24,
    margin:10
  }
});
}