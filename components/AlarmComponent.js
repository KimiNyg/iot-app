import React, { Component } from 'react';

import { StyleSheet, Text, View, Button, TouchableOpacity, Alert } from 'react-native';

export default class Alarm extends Component {

    deleteAlarm(id){
      return fetch('http://192.168.1.108:3000/api/alarmModel/delete', {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: id
        })
      }).then(()=>{Alert.alert(`Alarm deleted : ${id}`)})
      };

  render() {
    return (
        <View style={this.styles.container}>
          <Text style={this.styles.text}>{this.props.hour} : {this.props.minute}</Text>
          <Text style={this.styles.text}>{`Alarm on : ${this.props.active}`}</Text>
          <TouchableOpacity style={this.styles.close}
               onPress = {
                  () => this.deleteAlarm(this.props.id)
               }>
               <Text style={this.styles.closeX}> X </Text>
            </TouchableOpacity>
        </View>
      )
    }

    styles = StyleSheet.create({
        container: {
            alignItems: 'center',
            margin: 1,
            flexDirection:'row',
            justifyContent: 'center',
            backgroundColor: '#4C5454',
            width:'100%',
            alignSelf:'center', 
            height:60,
        },
        text: {
            color:'#fff',
            margin:10
        },
        close:{
          position:"absolute",
          right:30,
          fontWeight:"bold",
          color:'#FF715B'
        },
        closeX:{
          fontWeight:"bold",
          color:'rgb(255,0,0)'
        }
      });

  }